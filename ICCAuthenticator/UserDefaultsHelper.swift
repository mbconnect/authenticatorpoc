//
//  UserDefaultsHelper.swift
//  ICCAuthenticator
//
//  Created by Nathalie Bressa on 20.10.15.
//  Copyright © 2015 Nathalie Bressa. All rights reserved.
//

import Foundation


class UserDefaultsHelper: NSObject {
    
    static let userDefaults = UserDefaults.standard
    
    static func getString(_ identifier: String) -> String{
        
        if let returnValue = self.userDefaults.value(forKey: identifier){
            return returnValue as! String
        }
        else{
           return ""
        }
        
    }
    
    static func setString(_ identifier: String, string : String){
        self.userDefaults.setValue(string, forKey: identifier)
    }
    
}

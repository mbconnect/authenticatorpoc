//
//  HTTPManager.swift
//  ICCAuthenticator
//
//  Created by Nathalie Bressa on 28.10.15.
//  Extended by Dr. Ingo Pansa on 24.03.17.
//
//  Copyright © 2017 iC Consult GmbH. All rights reserved.
//

import Foundation
import Alamofire
import JWTDecode


class HTTPManager : NSObject{
  
    fileprivate let actionTokenRequestURL = "https://api-dev.corpinter.net/ats/ActionToken/";
    fileprivate let authenticatorTotpUrl = "https://api-dev.corpinter.net/authenticator/totp";
    fileprivate let startAuthenticationTestUrl = "https://api-dev.corpinter.net/authenticator/startAuthentication";
    fileprivate let enrollDeviceUrl = "https://api-dev.corpinter.net/userprofileservice/enrollment";
    
    fileprivate let authenticatorApikey = "gDNgrOZpUVUn9Mv4Jv6ikkRxx3WmjiK3";
    fileprivate let actionTokenApikey = "P8TKV331m2laiqBZIKRGBwHm71kveIzt";
    fileprivate let enrollmentApikey = "7ZYjOQy3lH6G168A3zBbrPSQlDVkeJFr";
    
    func enrollDevice(_ deviceToken: String, _ pushNotificationRecipientID: String, _ userName: String, completion: @escaping (_ error : NSError?, _ success : Bool?) -> Void){
        print("enrollDevice()");
        
        let deviceToken : String = UserDefaultsHelper.getString("deviceToken")
        
        let parameters = [
            "userName": userName,
            "deviceToken": deviceToken,
            "pushNotificationRecipientID":pushNotificationRecipientID 
        ]
        
        let headers: HTTPHeaders = [
            "apikey": enrollmentApikey,
            "Accept": "application/json"
        ]
        
        print("deviceToken: " + deviceToken)
        print("pushNotificationRecipientID: " + pushNotificationRecipientID)
        print("userName: " + userName)
        
        Alamofire.request(enrollDeviceUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON
            { response in
                
                if(response.result.error != nil){
                    print("POST error")
                    print(response.result.error)
                    completion(response.result.error as NSError?, nil)
                }else{
                    completion(nil, false)
//                    //Response
//                    if let JSON = response.result.value {
//                        print("POST response: \(JSON)")
//                        
//                        if let status = (JSON as AnyObject).value(forKey: "status") as? Int{
//                            
//                            if(status == 100){
//                                print("Transaction found")
//                                completion(nil, true)
//                            }
//                                
//                            else if(status == 200){
//                                print("Transaction not found")
//                                completion(nil, false)
//                            }
//                        }
//                    }
                }
        }

    }
    
    func sendActiontokenRequestWithTOTP( _ totp: Int, _ sub: String, completion: @escaping( _ error: NSError?, _ id : String?, _ actionToken: JWT?) -> Void){
        print("sendActiontokenRequestWithTOTP()");
        
        let deviceToken : String = UserDefaultsHelper.getString("deviceToken")
        
        let parameters = [
            "totp": String(totp),
            "sub": sub,
            "deviceToken": deviceToken,
            "aud":"MMe"
        ]
        
        let headers: HTTPHeaders = [
            "apikey": actionTokenApikey,
            "Accept": "application/json"
        ]
        
        Alamofire.request(actionTokenRequestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON
            { response in
                
                if(response.result.error != nil){
                    print("POST error")
                    print(response.result.error)
                    completion(response.result.error as NSError?, nil, nil)
                }else{
                    if let JSON = response.result.value {
                        print("GET response: \(JSON)")
                        
                        let id : String = (JSON as AnyObject).value(forKey: "id") as? String ?? ""
                        let actionToken : String = (JSON as AnyObject).value(forKey: "actionToken") as? String ?? ""
                        var jwt: JWT!
                        
                        do {
                            try jwt = decode(jwt: actionToken)
                        } catch _ {
                            print("error")
                        }
                        
                        //print(jwt.header)
                        //print(jwt.body)
                        //print(jwt.signature)
                        completion(nil, id, jwt)
                    }
                    
                    
                }
        };
    }
    
    //POST request: send TOTP and deviceToken - accepts authentication  process
    func sendTotp(_ totp : String, completion: @escaping (_ error : NSError?, _ success : Bool?) -> Void){
        print("sendTotp()");
        
        let deviceToken : String = UserDefaultsHelper.getString("deviceToken")
        
        let parameters = [
            "totp": totp,
            "deviceToken": deviceToken
        ]
        
        let headers: HTTPHeaders = [
            "apikey": authenticatorApikey,
            "Accept": "application/json"
        ]
        
        Alamofire.request(authenticatorTotpUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON
            { response in
            
            if(response.result.error != nil){
                print("POST error")
                print(response.result.error)
                completion(response.result.error as NSError?, nil)
            }else{
                //Response
                if let JSON = response.result.value {
                    print("POST response: \(JSON)")
                    
                    if let status = (JSON as AnyObject).value(forKey: "status") as? Int{
                        
                        if(status == 100){
                            print("Transaction found")
                            completion(nil, true)
                        }
                            
                        else if(status == 200){
                            print("Transaction not found")
                            completion(nil, false)
                        }
                    }
                }
            }
        }
    }

    
    //DELETE request: send deviceToken - decline authentication process
    func declineLogin(_ completion: @escaping (_ error : NSError?, _ success : Bool?) -> Void){
        print("declineLogin()");

        let parameters = [
            "deviceToken": UserDefaultsHelper.getString("deviceToken")
        ]
        
        let headers: HTTPHeaders = [
            "apikey": authenticatorApikey,
            "Accept": "application/json"
        ]
        
        Alamofire.request(authenticatorTotpUrl, method: .delete, parameters:parameters, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON
            { response in
        
            if(response.result.error != nil){
                print("DELETE error")
                print(response.result.error)
                completion(response.result.error as NSError?, nil)
            }
                
            else{

                if let JSON = response.result.value {
                    print("POST response: \(JSON)")
                    
                    
                    if let status = (JSON as AnyObject).value(forKey: "status") as? Int{
                        
                        //Successful
                        if(status == 100){
                            completion(nil, true)
                        }
                            
                        //Not successful - no error code right now
                        else{
                            completion(nil, false)
                        }
                    }
                }
            }
        }
    }
    
    
    //GET request: send deviceToken - check if there are active transactions for a deviceToken
    func checkForActiveTransaction(_ completion: @escaping (_ error : NSError?, _ activeTransaction : Bool?, _ loginname : String, _ userAgent: String, _ applicationName : String) -> Void){
        print("checkForActiveTransaction()");
        
        let parameters = [
            "deviceToken": UserDefaultsHelper.getString("deviceToken")
        ]
        
        let headers: HTTPHeaders = [
            "apikey": authenticatorApikey,
            "Accept": "application/json"
        ]
        
        //Alamofire.request(.GET, authenticatorTotpUrl, parameters: parameters).responseJSON
        Alamofire.request(authenticatorTotpUrl, method: .get, parameters:parameters, headers: headers)
            .validate(contentType: ["application/json"])
            .responseJSON
            { response in
            
            if(response.result.error != nil){
                print("GET error")
                print(response.result.error)
                completion(response.result.error! as NSError?, nil, "", "", "")
            }
                
            else{
                
                if let JSON = response.result.value {
                    print("GET response: \(JSON)")
                    
                    if let status = (JSON as AnyObject).value(forKey: "status") as? Int{
                        
                        var activeTransaction = false
                        
                        //Active transaction exists (status 200 = no active transaction)
                        if(status == 100){
                            activeTransaction = true
                        }
                        
                        //TODO: fix error
                       
                        let name : String = (JSON as AnyObject).value(forKey: "loginname") as? String ?? ""
                        let userAgent : String = (JSON as AnyObject).value(forKey: "userAgent") as? String ?? ""
                        let application : String = (JSON as AnyObject).value(forKey: "applicationName") as? String ?? ""
                            
                        completion(nil, activeTransaction, name, userAgent, application)
                    }
                }
            }
        }
    }
    
    
    //Only for testing purposes - start authentication process
    func startAuthenticationProcess(){
        print("startAuthenticationProcess()");

        let parameters = [
            "loginname": "nathalie.bressa@ic-consult.de",
            "deviceToken": UserDefaultsHelper.getString("deviceToken")
        ]
        
        let headers: HTTPHeaders = [
            "apikey": authenticatorApikey,
            "Accept": "application/json"
        ]
        
        //Alamofire.request(.POST, startAuthenticationTestUrl, parameters: parameters, encoding: .JSON).responseJSON 
        Alamofire.request(startAuthenticationTestUrl, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: headers)
                .validate(contentType: ["application/json"])
                .responseJSON
            { response in
            
            if(response.result.error != nil){
                print(response.result.error)
            }
                
            else{
                print("Authentication started.")
            }
        }
    }
    
//    //Quick Fix -remove this
//    func resetBadgeNumber(){
//        
//        let parameters = [
//            "badge": 0
//        ]
//        
//        let headers = [
//            "X-Parse-Application-Id": "MIEeDPtTsuvatqV5ryzQ5VgDguxQwqpS4b90gv36",
//            "X-Parse-REST-API-Key": "eyh6PGVTjNMsON4n4PMo4oBNXCxHG3Ad5kCf99ou"
//        ]
//        
//        Alamofire.request(updateInstallationUrl, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
//            
//            if(response.result.error != nil){
//                print(response.result.error)
//            }
//                
//            else{
//                print("Badge number reset.")
//            }
//        }
//
//        
//    }
    
    
}

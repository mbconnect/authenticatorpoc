//
//  TouchIDAuthScheme.swift
//  ICCAuthenticator
//
//  Created by Nathalie Bressa on 25.11.15.
//  Extended by Dr. Ingo Pansa on 24.03.17.
//
//  Copyright © 2017 iC Consult GmbH. All rights reserved.
//

import Foundation
import LocalAuthentication


class TouchIdManager : NSObject {
    
    private let context : LAContext = LAContext()
    
    enum TouchIdResult{
        
        case Success
        case Failure
        case Fallback
        case TouchIdNotAvailable
        
    }
    
    
    func authenticateWithTouchId(completion: @escaping (TouchIdResult) -> Void){
        
        if(self.isTouchIdAvailbale()){
            
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: NSLocalizedString("TOUCH_ID_MESSAGE", comment: ""), reply: { (success, error) -> Void in
                
                if(success){
                    completion(TouchIdResult.Success)
                }
                
                if(error != nil){
                    
                    switch error?._code{
                        
                    case LAError.authenticationFailed.rawValue? :
                        print("Authentication failed.")
                        completion(TouchIdResult.Failure)
                        break
                        
                    case LAError.userFallback.rawValue? :
                        print("Fallback method selected.")
                        completion(TouchIdResult.Fallback)
                        break
                        
                    case LAError.userCancel.rawValue? :
                        print("Authentication canceled by user.")
                        completion(TouchIdResult.Failure)
                        break
                        
                    case LAError.systemCancel.rawValue? :
                        print("Authentication canceled by system.")
                        completion(TouchIdResult.Failure)
                        break
                        
                    case LAError.touchIDNotEnrolled.rawValue? :
                        print("Touch ID has no enrolled fingers.")
                        completion(TouchIdResult.Failure)
                        break
                        
                    case LAError.touchIDLockout.rawValue? :
                        print("Touch ID retry limit exceeded.")
                        completion(TouchIdResult.Failure)
                        break
                        
                    default :
                        print("Authentication failed.")
                        completion(TouchIdResult.Failure)
                        break
                        
                    }
                }
            })
        }
            
        else{
            completion(TouchIdResult.TouchIdNotAvailable)
        }
    }
    
    
    func isTouchIdAvailbale() -> Bool{
        
        if(context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil)){
            return true
        }
            
        else{
            return false
        }
    }
    
}

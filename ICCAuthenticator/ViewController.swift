//
//  ViewController.swift
//  ICCAuthenticator
//
//  Created by Nathalie Bressa on 19.10.15.
//  Extended by Dr. Ingo Pansa on 24.03.17.
//
//  Copyright © 2017 iC Consult GmbH. All rights reserved.
//

import UIKit
import Alamofire
import LocalAuthentication

class ViewController: UIViewController {
    
    fileprivate let httpManager : HTTPManager = HTTPManager()
    fileprivate let touchId : TouchIdManager = TouchIdManager()
    fileprivate let totpGenerator : TotpGenerator = TotpGenerator()
    
    @IBOutlet weak var infoTextLabel: UILabel!
    @IBOutlet weak var buttonsView: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.willEnterForground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        self.buttonsView.isHidden = true
        self.infoTextLabel.text = NSLocalizedString("START_LOGIN", comment: "")
        
        //Only for Testing purposes
        //httpManager.startAuthenticationProcess()
    }
    
    
    //Check for active transaction when app is opened from background
    func willEnterForground() {
        self.checkForActiveTransaction()
    }
    
    //Check for active transaction when app is launched
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkForActiveTransaction()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    //Refresh - check for active transactions
    @IBAction func refresh(_ sender: AnyObject) {
        self.checkForActiveTransaction()
    }
    
    
    //Decline login process
    @IBAction func decline(_ sender: AnyObject) {
        
        httpManager.declineLogin{ (error, success) -> Void in
            
            if(error != nil){
                if let errorMessage : String = error?.localizedDescription{
                    self.view.makeToast(message: "Error: \(errorMessage)")
                }
            }
            
            else{
                
                if(success == true){
                     self.view.makeToast(message: NSLocalizedString("DECLINE_LOGIN_SUCCESSFUL", comment: ""))
                }
                
                else{
                    self.view.makeToast(message: "Error.")
                }
                
                self.checkForActiveTransaction()
            }
        }
    }
    
    
    //Accept login process 
    @IBAction func accept(_ sender: AnyObject) {
        authenticateWithTouchId()
    }
    
    
    func authenticateWithTouchId(){
        touchId.authenticateWithTouchId { (result) -> Void in
            switch result{
                
            case .Success:
                self.acceptLogin()
                break
                
            case .Failure :
                break
                
            case .Fallback :
                self.showPinAlert()
                break
                
            case .TouchIdNotAvailable :
                self.showAlert(NSLocalizedString("TOUCH_ID_NOT_AVAILABLE_TITLE", comment: ""), message: NSLocalizedString("TOUCH_ID_NOT_AVAILABLE_MESSAGE", comment: ""))
                break
            }
        }
    }
    
    
    func acceptLogin(){   
        
        if let totp = totpGenerator.calculateCode(){
            
            httpManager.sendTotp(String(totp), completion: { (error, success) -> Void in
                
                if(error != nil){
                    if let errorMessage : String = error?.localizedDescription{
                        self.view.makeToast(message: "Error: \(errorMessage)")
                    }
                }
                    
                else{
                    
                    if(success == true){
                        self.view.makeToast(message: NSLocalizedString("ACCEPT_LOGIN_SUCCESSFUL", comment: ""))
                    }
                        
                    else{
                        self.view.makeToast(message: NSLocalizedString("ACCEPT_LOGIN_NO_TRANSACTION", comment: ""))
                    }
                    
                    self.checkForActiveTransaction()
                }
            })

        }
        
        else{
            self.showAlert("Key Wrong Format", message: "The key needs to be Base32 encoded.")
        }
    }
    
    
    func checkForActiveTransaction(){
        print("checkForActiveTransaction")
        
        httpManager.checkForActiveTransaction { (error, activeTransaction, loginname, userAgent, applicationName) -> Void in
            
            if(error != nil){
                //Show some error?
                self.buttonsView.isHidden = true
                self.infoTextLabel.text = NSLocalizedString("START_LOGIN", comment: "")
            }
                
            else{
                
                //Transaction found – show buttons/hide text
                if (activeTransaction == true){
                    self.buttonsView.isHidden = false
                    self.infoTextLabel.text = "Login requested for\n\(loginname)\n\nLogged in from : \(userAgent)\n\nApplication: \(applicationName)"
                }
                
                //No transaction found - hide buttons/show text
                else{
                    
                    self.buttonsView.isHidden = true
                    self.infoTextLabel.text = NSLocalizedString("START_LOGIN", comment: "")
                }
            }
        }
    }
    
    
    func showAlert(_ title : String, message : String){
        print("showAlert")
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    

    func showPinAlert() {
        print("showPinAlert")
        
        var inputTextField: UITextField?
        
        let alert = UIAlertController(title: NSLocalizedString("ENTER_PIN_TITLE", comment: ""), message: NSLocalizedString("ENTER_PIN_MESSAGE", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            //PIN correct
            if(KeychainWrapper.stringForKey("pin") == inputTextField?.text){
                
                print("PIN correct.")
                self.acceptLogin()
                
            }
            
            //PIN incorrect
            else{
                print("PIN incorrect.")
                self.showPinAlert()
            }
        }
        
        alert.addAction(OKAction)
        
        alert.addTextField { (textField : UITextField) -> Void in
            
            textField.isSecureTextEntry = true
            textField.keyboardType = UIKeyboardType.numberPad
            inputTextField = textField
            
        }
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    
}



//
//  TotpAuthScheme.swift
//  ICCAuthenticator
//
//  Created by Nathalie Bressa on 21.10.15.
//  Extended by Dr. Ingo Pansa on 24.03.17.
//
//  Copyright © 2017 iC Consult GmbH. All rights reserved.
//

import Foundation


class TotpGenerator: NSObject {
    
    fileprivate let digestLength = CC_SHA1_DIGEST_LENGTH
    fileprivate let hmacAlgorithm = CCHmacAlgorithm(kCCHmacAlgSHA1)
    fileprivate let period : TimeInterval = 30
    fileprivate let secretKeyLength : UInt32 = 1000 * 1000
    
    func calculateCode() -> UInt32?{
        
        if let key = getKey(){
            return calculateCode(key)
        }
        
        return nil
    }
    
    fileprivate func getKey() -> Data?{
        
        if let key = KeychainWrapper.stringForKey("secret")?
            .data(using: String.Encoding.utf8)?.base32DecodedData{
            return key
        }
    
        return nil
        
    }
    
    
    fileprivate func calculateCode(_ key : Data) -> UInt32{

        //result data
        let result : NSMutableData = NSMutableData(length: Int(digestLength))!
    
        //Counter for HMAC, calculated from Timestamp
        var counter = calculateCounterFromTimestamp()
        //let counterData : Data = Data(bytes: UnsafePointer<UInt8>(&counter), count: sizeofValue(counter))
        let counterData = Data(buffer: UnsafeBufferPointer(start: &counter, count: MemoryLayout.size(ofValue: counter)))
        
        
        //OTP with HMAC calculated with counter and key
        CCHmac(hmacAlgorithm, (key as NSData).bytes, key.count, (counterData as NSData).bytes, counterData.count, result.mutableBytes)
        print("HMAC : \(result)")
        
        //Truncate the HMAC
        let truncatedHash = truncate(result)
        
        //6 digit Pin
        let pin = truncatedHash % secretKeyLength
        print("Pin: \(pin)")
        
        return pin
        
    }
    
    
    fileprivate func calculateCounterFromTimestamp() -> UInt64{
        
        let date : Date = Date()
        let seconds : TimeInterval = date.timeIntervalSince1970
        
        var counter : UInt64 = UInt64(seconds/self.period)
        counter = NSSwapHostLongLongToBig(counter)
        
        return counter
        
    }
    
    
    fileprivate func calculateTruncateOffset(_ data : NSMutableData) -> Int{
        
        let offsetRange : NSRange = NSMakeRange(data.length-1, 1)
        let offsetData : Data = data.subdata(with: offsetRange)
        
        var offset: Int = 0
        (offsetData as NSData).getBytes(&offset, length: MemoryLayout<Int>.size)
        offset &= 0x0f
        
        return offset
        
    }
    

    fileprivate func truncate(_ data : NSMutableData) -> UInt32{
        
        let truncatedHashRange : NSRange = NSMakeRange(calculateTruncateOffset(data), 4)
        let truncatedHashData : Data = data.subdata(with: truncatedHashRange)
        
        var truncatedHash: UInt32 = 0
        (truncatedHashData as NSData).getBytes(&truncatedHash, length: MemoryLayout<UInt32>.size)
        truncatedHash = NSSwapBigIntToHost(truncatedHash)
        truncatedHash &= 0x7fffffff
        print("Truncated Hash: \(truncatedHash)")
        
        return truncatedHash

    }
    
    
}


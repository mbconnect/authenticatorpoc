//
//  LoginViewController.swift
//  ICCAuthenticator
//
//  Created by Ingo Pansa on 3/15/17.
//
//  Copyright © 2017 iC Consult GmbH. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(KeychainWrapper.stringForKey("username") == nil || KeychainWrapper.stringForKey("username") == ""){
            KeychainWrapper.setString(NSLocalizedString("username", comment: ""), forKey: "username")
        }
        
        usernameLabel.text = KeychainWrapper.stringForKey("username")
        
        if(KeychainWrapper.stringForKey("password") == nil || KeychainWrapper.stringForKey("password") == ""){
            KeychainWrapper.setString(NSLocalizedString("password", comment: ""), forKey: "password")
        }
        
        passwordLabel.text = KeychainWrapper.stringForKey("password")
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        KeychainWrapper.setString(usernameLabel.text!, forKey: "username")
        KeychainWrapper.setString(passwordLabel.text!, forKey: "password")
        
        self.navigationController!.viewControllers.removeAll()
    }

    
  
}

//
//  EnrollmentViewController.swift
//  ICCAuthenticator
//
//  Created by Ingo Pansa on 3/10/17.
//
//  Copyright © 2017 iC Consult GmbH. All rights reserved.
//

import Foundation
import UIKit
import OneSignal

class EnrollmentViewController: UIViewController {
    fileprivate let httpManager : HTTPManager = HTTPManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var playerID = "0"
        
        OneSignal.idsAvailable { (pushID, pushToken) in
            playerID = pushID!
//            print(pushToken)
        }


        httpManager.enrollDevice(
            UserDefaultsHelper.getString("deviceToken"),
            playerID,
            KeychainWrapper.stringForKey("username")!,
            completion:{(error, success) -> Void in
        })
    }

}

//
//  DemoAppController.swift
//  ICCAuthenticator
//
//  Created by Ingo Pansa on 3/24/17.
//
//  Copyright © 2017 iC Consult GmbH. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication
import JWTDecode

class DemoAppController: UIViewController {
    fileprivate let httpManager : HTTPManager = HTTPManager()
    fileprivate let touchId : TouchIdManager = TouchIdManager()
    fileprivate let totpGenerator : TotpGenerator = TotpGenerator()

    
    @IBOutlet weak var actionTokenLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        print("DemoAppController.viewDidAppear()")
        self.initializeActionTokenLabel()
    }
    
    override func viewDidLoad() {
        print("DemoAppController.viewDidLoad()")
        super.viewDidLoad()
    }
    
    private func initializeActionTokenLabel() {
        if let actionToken = KeychainWrapper.stringForKey("actionToken") {
            do {
                // convert ActionToken from String to JWT instance
                let jwt: JWT!
                try jwt = decode(jwt: actionToken)
                
                // build result string (poor man's JSON builder...)
                var result: String = String("{\n")
                for item in jwt.body {
                    result.append("\t\"")
                    result.append(item.key)
                    result.append("\"")
                    result.append(": ")
                    if let value = Int(item.value as! String) {
                        result.append(String(value))
                    } else {
                        result.append("\"")
                        result.append(item.value as! String)
                        result.append("\"")
                    }
                    result.append(",\n")
                }
                result.append("}")
                actionTokenLabel.text = result
            } catch _ {
                actionTokenLabel.text = "The received Action Token was invalid. Please try again."
                print("Error decoding JWT!")
            }
        } else {
            actionTokenLabel.text = "No Action Token received yet"
        }
    }
    
    @IBAction func doorUnlock(_ sender: UIButton) {
        print("DemoAppController.doorUnlock()")
        
        if(KeychainWrapper.stringForKey("actionToken") == nil || KeychainWrapper.stringForKey("actionToken") == ""){
            authenticateWithTouchId()
            // store this action token in the keychain
            
            // call dummy function with freshly created action token
        } else {
            let alertController = UIAlertController(title: "ActionToken", message:
                "Before requesting a new ActionToken, please delete the current one under the settings pane and try again.", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    func authenticateWithTouchId(){
        touchId.authenticateWithTouchId { (result) -> Void in
            switch result{
                
            case .Success:
                print("authenticateWithTouchId(): Success")
                self.requestActionToken()
                break
                
            case .Failure :
                print("authenticateWithTouchId(): failure")
                break
                
            case .Fallback :
                print("authenticateWithTouchId(): fallback")
                break
                
            case .TouchIdNotAvailable :
                print("authenticateWithTouchId(): TouchIdNotAvailable")
                self.requestActionToken()
                break
            }
        }
    }
    
       
    func requestActionToken(){
        
        if let totp = totpGenerator.calculateCode(){
            
            httpManager.sendActiontokenRequestWithTOTP(Int(totp), "me@me.com", completion: { (error, id, actionToken) -> Void in
                if(error != nil){
                    print("1")
                    if let errorMessage : String = error?.localizedDescription{
                        let alertController = UIAlertController(title: "MFAuthN", message:
                            errorMessage, preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    }
                }else{
                    print(id)
                    let alertController = UIAlertController(title: "MFAuthN", message:
                         id, preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    
                    KeychainWrapper.setString((actionToken?.string)!, forKey: "actionToken")
                    self.initializeActionTokenLabel()
                }
            })
            
        }
            
        else{
            
        }
    }
}

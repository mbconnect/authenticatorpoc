//
//  SettingsViewController.swift
//  ICCAuthenticator
//
//  Created by Nathalie Bressa on 20.10.15.
//  Extended by Dr. Ingo Pansa on 24.03.17.
//
//  Copyright © 2017 iC Consult GmbH. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController, UITextFieldDelegate {

//    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var deviceTokenLabel: UILabel!
    @IBOutlet weak var actionTokenLabel: UILabel!
    @IBOutlet weak var secretTextField: UITextField!
    @IBOutlet weak var pinTextField: UITextField!
    
    private let numbersOfPin = 6
    private var keyboardHeight : CGFloat = 0.0
    
    override func viewDidAppear(_ animated: Bool) {
        self.actionTokenLabel.text = KeychainWrapper.stringForKey("actionToken")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.keyboardHeight = self.keyboardHeightLayoutConstraint.constant
        self.deviceTokenLabel.text = UserDefaultsHelper.getString("deviceToken")
        self.secretTextField.delegate = self
        self.pinTextField.delegate = self
        
        if (UIDevice.current.modelName == "iPhone 6" || UIDevice.current.modelName == "iPhone 6s" || UIDevice.current.modelName == "iPhone 6 Plus" || UIDevice.current.modelName == "iPhone 6s Plus") {
            self.keyboardHeight = 170
//            self.keyboardHeightLayoutConstraint.constant = 170            
        }
        
        //Set default values when no values are set
        if(KeychainWrapper.stringForKey("secret") == nil || KeychainWrapper.stringForKey("secret") == ""){
            KeychainWrapper.setString(NSLocalizedString("DEFAULT_SECRET", comment: ""), forKey: "secret")
        }
        
        if(KeychainWrapper.stringForKey("pin") == nil || KeychainWrapper.stringForKey("pin") == ""){
            
            var pin : String = ""
            
            for i in 0 ..< self.numbersOfPin{
                pin += String(i+1)
            }
            KeychainWrapper.setString(pin, forKey: "pin")
        }
        
        //Set values from keychain to textfields
        self.secretTextField.text = KeychainWrapper.stringForKey("secret")
        self.pinTextField.text = KeychainWrapper.stringForKey("pin")
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.keyboardNotification), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        saveSecretToKeyChain()
        savePinToKeychain()
    }
    
    
    func saveSecretToKeyChain(){
        
        if(KeychainWrapper.stringForKey("secret") != self.secretTextField.text){
            KeychainWrapper.setString(self.secretTextField.text!, forKey: "secret")
        }
    }
    
    
    func savePinToKeychain(){
        
        if(KeychainWrapper.stringForKey("pin") != self.pinTextField.text){
             KeychainWrapper.setString(self.pinTextField.text!, forKey: "pin")
        }
    }
    
    @IBAction func copyDeviceToken(_ sender: AnyObject) {
        UIPasteboard.general.string = self.deviceTokenLabel.text
    }
    
    @IBAction func copyActiontoken( _sender: AnyObject) {
        UIPasteboard.general.string = self.actionTokenLabel.text
    }
    
    @IBAction func deleteActionToken( _sender: AnyObject) {
        self.actionTokenLabel.text = ""
        KeychainWrapper.removeObjectForKey("actionToken")
    }

    
    //Return on Keyboard pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //Dismiss Keyboard when the user touches the view outside of keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //Limit possible numbers of PIN in pinTextfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == self.pinTextField){
            
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= numbersOfPin
        }
        return true
    }
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            //self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
//                self.keyboardHeightLayoutConstraint?.constant = self.keyboardHeight
            }
            else {
//                self.keyboardHeightLayoutConstraint?.constant = endFrame!.size.height
            }
            
            UIView.animate(withDuration: duration,
                delay: TimeInterval(0),
                options: animationCurve,
                animations: { self.view.layoutIfNeeded() },
                completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }



}
